import fire
import random
import torch
import torch.nn as nn
import torch.optim as optim

from torch.nn.utils.rnn import (pack_padded_sequence, pad_packed_sequence)
from torch.utils.tensorboard import SummaryWriter

from torchtext import data, vocab
from sklearn.metrics import precision_recall_fscore_support
from sklearn.model_selection import ParameterGrid

from tqdm import tqdm
from typing import NamedTuple
from pathlib import Path
from copy import deepcopy

# Preparing seed
random.seed(1234)
torch.manual_seed(1234)
torch.cuda.manual_seed_all(1234)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
DEVICE = "cpu"
if torch.cuda.is_available():
    DEVICE = "cuda"


###############################################################################
# Embedding Model
###############################################################################
class NormalEmbedding(nn.Module):
    class Hyperparams(NamedTuple):
        embedding_size: int = 300
        dropout: float = 0.6

    def __init__(self, input_size: int, params: Hyperparams):
        super().__init__()

        self._emb = nn.Sequential(
            nn.Embedding(input_size, params.embedding_size),
            # nn.LayerNorm(params.embedding_size))
            nn.Dropout(params.dropout))

    def forward(self, words: torch.Tensor):
        return self._emb(words)


###############################################################################
# Encoder Model
###############################################################################
class LSTMEncoder(nn.Module):
    class Hyperparams(NamedTuple):
        hidden_size: int = 128
        num_layers: int = 1
        dropout: float = 0.5
        bidirectional: bool = True

    def __init__(self, input_size: int, params: Hyperparams):
        super().__init__()

        self._encoder = nn.LSTM(
            input_size,
            params.hidden_size,
            params.num_layers,
            dropout=params.dropout if params.num_layers > 1 else 0.0,
            bidirectional=params.bidirectional)
        self._bidirectional = params.bidirectional
        self.init_weights()

    def forward(self, embedding: torch.Tensor, seq_len: torch.Tensor):
        packed_emb = pack_padded_sequence(embedding, seq_len)
        out, _ = self._encoder(packed_emb)
        out, _ = pad_packed_sequence(out)

        if self._bidirectional:
            # Resizing output to separate forward and backward output
            out = out.view(out.size(0), out.size(1), 2, -1)

            # Since we are doing classification, we only need the last
            # output from RNN (and first for bidirectional)
            output_last_timestep = (
                out[seq_len -
                    1, torch.arange(embedding.size(1)), 0],
                out[0, torch.arange(embedding.size(1)), 1])

            # Concat forward and backward LSTM outputs
            output_last_timestep = torch.cat(output_last_timestep, dim=-1)
        else:
            output_last_timestep = out[seq_len - 1,
                                       torch.arange(embedding.size(1))]
        return output_last_timestep

    def init_weights(self):
        for n, p in self._encoder.named_parameters():
            if "bias" not in n:
                nn.init.orthogonal_(p, gain=nn.init.calculate_gain("tanh"))
            else:
                nn.init.zeros_(p)


###############################################################################
# Decoder Model
###############################################################################
class LinearDecoder(nn.Module):
    class Hyperparams(NamedTuple):
        output_size: int = 1
        hidden_size: int = 128

    def __init__(self, input_size: int, params: Hyperparams):
        super().__init__()

        self.decoder = nn.Sequential(nn.LayerNorm(input_size),
                                     nn.Linear(input_size, params.output_size))

    def forward(self, words: torch.Tensor):
        return self.decoder(words)

    def init_weights(self):
        for n, p in self._encoder.named_parameters():
            if "bias" not in n:
                nn.init.xavier_uniform_(p)
            else:
                nn.init.zeros_(p)


###############################################################################
# Classifier Model
###############################################################################
class Classifier(nn.Module):
    def __init__(self, embedding: nn.Module, encoder: nn.Module,
                 decoder: nn.Module):
        super().__init__()

        self.embedding = embedding
        self.encoder = encoder
        self.decoder = decoder

    def forward(self, words: torch.Tensor, seq_len: torch.Tensor):
        out = self.embedding(words)
        out = self.encoder(out, seq_len)
        out = self.decoder(out)
        return out


###############################################################################
# Dataset
##############################################################################
def load_dataset(path: Path,
                 min_freq: int = 5,
                 batch_size: int = 32,
                 pretrained: bool = False,
                 device: str = "cpu"):

    text = data.Field(include_lengths=True)
    sentiment = data.LabelField(pad_token=None, dtype=torch.float32)
    train_ds, eval_ds, test_ds = data.TabularDataset.splits(
        path,
        root="data",
        train="clean_train_data_restaurant.tsv",
        validation="clean_eval_data_restaurant.tsv",
        test="clean_test_data_restaurant.tsv",
        format="tsv",
        skip_header=True,
        fields=[("text", text), ("sentiment", sentiment)])

    if pretrained:
        fasttext_vector = vocab.FastText("id")
    else:
        fasttext_vector = None
    text.build_vocab(train_ds.text,
                     eval_ds.text,
                     vectors=fasttext_vector,
                     min_freq=min_freq,
                     max_size=80000)
    sentiment.build_vocab(train_ds.sentiment, eval_ds.sentiment)
    # Save field
    torch.save(text, "model/text_field.pt")
    torch.save(sentiment, "model/sentiment_field.pt")

    train_iter, eval_iter, test_iter = data.BucketIterator.splits(
        datasets=[train_ds, eval_ds, test_ds],
        batch_size=batch_size,
        sort_within_batch=True,
        sort_key=lambda x: len(x.text),
        device=device,
        repeat=False)

    return {
        "iterators": [train_iter, eval_iter, test_iter],
        "corpus": {
            "review": text,
            "sentiment": sentiment
        }
    }


###############################################################################
# Utils
##############################################################################
def logits_to_binary(logits: torch.Tensor, threshold: float = 0.5):
    y_pred = torch.sigmoid(logits)
    y_pred[y_pred >= threshold] = 1.
    y_pred[y_pred < threshold] = 0.
    return y_pred.squeeze(1)


def save_model(model: nn.Module, path: Path):
    torch.save(model.state_dict(), path)


def load_model(path: Path):
    return torch.load(path, lambda x, y: x)


def dict_to_str(d: dict):
    result = []
    for k, v in d.items():
        result.append("{}_{}".format(k, v))
    return "_".join(result)


###############################################################################
# Training
##############################################################################
def model_fn(param_grid: dict, num_vocab: int) -> nn.Module:
    """Function to build the whole model for sentiment analysis

    Args:
        param_grid (dict): Dictionary of hyperparameter
        num_vocab (int): Number of vocabularies from sentiment corpus

    Returns:
        nn.Module: Sentiment analysis model
    """
    embedding_params = NormalEmbedding.Hyperparams(
        embedding_size=param_grid["embedding_size"])
    encoder_params = LSTMEncoder.Hyperparams(
        hidden_size=param_grid["lstm_hidden_size"],
        bidirectional=param_grid["lstm_bidirectional"])

    embedding = NormalEmbedding(num_vocab, embedding_params)
    encoder = LSTMEncoder(embedding_params.embedding_size, encoder_params)
    encoder_hidden_size = encoder_params.hidden_size * 2 \
        if encoder_params.bidirectional else \
        encoder_params.hidden_size
    decoder = LinearDecoder(encoder_hidden_size, LinearDecoder.Hyperparams(1))
    model = Classifier(embedding, encoder, decoder)
    return model


def train_fn(model: nn.Module, opt: optim.Optimizer, criterion: nn.Module,
             iterable_dataset: data.Iterator) -> float:
    """Function to train sentiment analysis model

    Args:
        model (nn.Module): Pytorch module
        opt (optim.Optimizer): Optimizer
        criterion (nn.Module): Loss function
        iterable_dataset (data.Iterator): Training data iterator

    Returns:
        float: Average loss of the model after one iteration on the whole data
    """
    avg_loss = 0.0
    total_batch = len(iterable_dataset)
    model.train()

    for datum in tqdm(iterable_dataset):
        opt.zero_grad()
        text, y = datum.text, datum.sentiment
        words, seq_len = text

        logits = model(words, seq_len)
        loss = criterion(logits, y.unsqueeze(1))
        loss.backward()
        opt.step()

        avg_loss += (loss.item() / total_batch)

    return loss.item()


def test_fn(model: nn.Module, criterion: nn.Module,
            iterable_dataset: data.Iterator) -> dict:
    """Function to run testing of the trained model

    Args:
        model (nn.Module): Trained model
        criterion (nn.Module): Loss function
        iterable_dataset (data.Iterator): Test/Eval data
        Iterator

    Returns:
        dict: Metrics of model's performance (f1, precision, recall, and loss)
    """
    avg_loss = 0.0
    total_batch = len(iterable_dataset)
    true_labels = []
    pred_labels = []

    model.eval()

    with torch.no_grad():
        for datum in iterable_dataset:
            text, y = datum.text, datum.sentiment
            words = text[0]
            seq_len = text[1]

            logits = model(words, seq_len)
            loss = criterion(logits, y.unsqueeze(1))
            y_pred = logits_to_binary(logits)

            pred_labels.extend(y_pred.tolist())
            true_labels.extend(y.tolist())
            avg_loss += (loss.item() / total_batch)

    precision, recall, f1, support = precision_recall_fscore_support(
        true_labels, pred_labels, average="micro")

    return {
        "f1_score": f1,
        "precision_score": precision,
        "recall_score": recall,
        "loss": avg_loss,
    }


class Main(object):
    def demo(self, vocab_dir: str, model_path: str) -> None:
        """Demo script for sentiment analysis

        Args:
            vocab_dir (str): Directory where previously trained vocabulary
                resides
            model_path (str): File path where the trained model resides
        """

        text_field_path = Path(vocab_dir) / "text_field.pt"
        sentiment_field_path = Path(vocab_dir) / "sentiment_field.pt"
        model_path = Path(model_path)
        if not text_field_path.exists() or not sentiment_field_path.exists():
            raise FileNotFoundError(
                "Please check the availabiliy of text_field.pt "
                "and sentiment_field.pt in {}".format(vocab_dir))
        if not model_path.exists():
            raise FileNotFoundError(
                "Please check if {} is truly exist".format(model_path))

        text_field = torch.load(text_field_path)
        sentiment_field = torch.load(sentiment_field_path)
        model_weight = load_model(model_path)

        # Best hyperparams according to grid search
        hp = {
            "min_freq": 5,
            "batch_size": 128,
            "embedding_size": 300,
            "lstm_hidden_size": 64,
            "lstm_bidirectional": True,
        }
        num_vocab = len(text_field.vocab.itos)
        model = model_fn(hp, num_vocab)
        model.load_state_dict(model_weight)

        while True:
            query = input("Insert query sample: ")
            text = text_field.preprocess(query)
            text = text_field.numericalize(([text], [len(text)]))
            words = text[0]
            seq_len = text[1]

            # Stop demo if no inpupt available
            if query == "":
                return

            logits = model(words, seq_len)
            prediction = logits_to_binary(logits).item()
            sentiment = sentiment_field.vocab.itos[int(prediction)]

            print("Sentiment prediction: ", sentiment)
            print()

    def train(self, dataset_path: str, epoch: int):
        """Training module for sentiment analysis

        Args:
            dataset_path (str): Folder where the dataset resides.
            It has to contain clean_[eval|train|test]_data_restaurant.tsv
            epoch (int): Number of epochs
        """
        # List of hyperparameters that will be tried
        # grid = {
        #     "min_freq": [5],
        #     "batch_size": [16, 32, 64, 128, 1024],
        #     "embedding_size": [16, 32, 64, 300],
        #     "lstm_hidden_size": [16, 32, 64, 256],
        #     "lstm_bidirectional": [True, False],
        #     "pretrained": [True, False],
        #     "lr": [0.001, 0.01],
        # }

        # Best hyperparams
        grid = {
            "min_freq": [5],
            "batch_size": [128],
            "embedding_size": [300],
            "lstm_hidden_size": [64],
            "lstm_bidirectional": [True],
            "pretrained": [False],
            "lr": [0.001],
        }
        grid_combs = ParameterGrid(grid)
        with SummaryWriter("sentiment_analysis_experiments") as logger:
            for param_grid in grid_combs:
                # Skip training if embedding size is not 300 and
                # using pretrained vectors
                if param_grid["pretrained"] and \
                   param_grid["embedding_size"] != 300:
                    continue

                dataset = load_dataset(Path(dataset_path),
                                       min_freq=param_grid["min_freq"],
                                       batch_size=param_grid["batch_size"],
                                       pretrained=param_grid["pretrained"],
                                       device=DEVICE)
                train_iter, eval_iter, test_iter = dataset["iterators"]
                corpus = dataset["corpus"]

                # Model instantiation
                num_vocab = len(corpus["review"].vocab.itos)
                model = model_fn(param_grid, num_vocab)
                # Copy fasttext pretrained embedding
                if param_grid["pretrained"]:
                    model.embedding._emb[0].weight.data.copy_(
                        corpus["review"].vocab.vectors)

                opt = optim.Adam(model.parameters(), lr=param_grid["lr"])
                criterion = nn.BCEWithLogitsLoss()

                model.to(DEVICE)
                criterion.to(DEVICE)

                best_model = None
                best_f1_score = 0.0
                best_prec_score = 0.0
                best_rec_score = 0.0

                for curr_epoch in range(epoch):
                    print("Epoch {}".format(curr_epoch + 1))
                    train_result = train_fn(model, opt, criterion, train_iter)
                    print("Train -> Loss: {}".format(train_result))

                    eval_result = test_fn(model, criterion, eval_iter)
                    f1 = eval_result["f1_score"]
                    precision = eval_result["precision_score"]
                    recall = eval_result["recall_score"]
                    loss = eval_result["loss"]
                    print(
                        "Eval -> F1: {}, Precision: {}, Recall: {}, Loss: {}".
                        format(f1, precision, recall, loss))

                    # Selecting best performing model
                    if f1 > best_f1_score:
                        best_f1_score = f1
                        best_prec_score = precision
                        best_rec_score = recall
                        best_model = deepcopy(model)
                        print("Replace best model...")
                    elif f1 == best_f1_score:
                        if precision > best_prec_score:
                            best_prec_score = precision
                            best_model = deepcopy(model)
                            print("Replace best model...")
                        elif precision == best_prec_score:
                            if recall > best_rec_score:
                                best_rec_score = recall
                                best_model = deepcopy(model)
                                print("Replace best model...")

                # Reload best performing model
                save_model(
                    best_model,
                    "model/sentiment_{}.pt".format(dict_to_str(param_grid)))
                model.load_state_dict(
                    load_model(
                        Path("model/sentiment_{}.pt".format(
                            dict_to_str(param_grid)))))

                # Running test data
                test_result = test_fn(model, criterion, test_iter)
                logger.add_hparams(param_grid, test_result)
                f1 = test_result["f1_score"]
                precision = test_result["precision_score"]
                recall = test_result["recall_score"]
                loss = test_result["loss"]

                print("Test -> F1: {}, Precision: {}, Recall: {}, Loss: {}".
                      format(f1, precision, recall, loss))


if __name__ == "__main__":
    fire.Fire(Main)
