# prosa-nlp-aieng

## Repository structure

- data [This is the Folder to store training, evaluation, and testing data]
- model [This is the folder to store trained model (including vocabularies)]
- sentiment_analysis_experiments [Tensorboard folder that consists of list of experiments]
- Analysis.ipynb [Jupyter notebook containing data analysis and baseline model]
- dl_model_experiments.py [Python file containing deep learning model, training and demo codes]
- environment.yml [Conda dependency packages]
- Analysis.pdf [Summary of dataset, baseline, and deep learning model analysis]

## How to start

1. Install conda
2. Run `conda env create -f environment.yml`

## How to run experiments

As a default the experiments will only run the experiment with the best performing hyperparameters after a quite exhaustive grid search.

To run the experiment, please find the command below

```bash
python dl_model_experiments.py train --dataset-path data --epoch 30
```

If you are really interested to run the *whole* experiments with grid search, please uncomment the following lines on `dl_model_experiments.py`

```py
grid = {
    "min_freq": [5],
    "batch_size": [16, 32, 64, 128, 1024],
    "embedding_size": [16, 32, 64, 300],
    "lstm_hidden_size": [16, 32, 64, 256],
    "lstm_bidirectional": [True, False],
    "pretrained": [True, False],
    "lr": [0.001, 0.01],
}
```

And comment these lines

```py
grid = {
    "min_freq": [5],
    "batch_size": [128],
    "embedding_size": [300],
    "lstm_hidden_size": [64],
    "lstm_bidirectional": [True],
    "pretrained": [False],
    "lr": [0.001],
}
```

To see the experiments result, run tensorboard as follows

```bash
tensorboard --logdir sentiment_analysis_experiments
```

## How to run demo

Please execute the following command

```bash
python dl_model_experiments.py demo --vocab-dir model --model-path model/sentiment_batch_size_128_embedding_size_300_lr_0.001_lstm_bidirectional_True_lstm_hidden_size_64_min_freq_5_pretrained_False.pt
```

On every turn, users will be asked to enter a query that later will be predicted by the deep learning model. The output will be as follows

```
Insert query sample: menu yang ditawarkan di tempat ini bikin jadi lapar dengan bau aroma ikan bakar dan ayam bakarnya gurame gorengnya sungguh mantap sekali ditambah dengan sambal yang khas sunda asli tempat nya bersih dan pelayanan nya bagus sekali lain kali kita akan kemari lagi tunggu ya
Sentiment prediction:  positive

Insert query sample: mencoba mencari alternatif lain bakmie yg blm pernah dicoba kali ini mencoba bakmie anugrahkami mencoba di area food court citylink fesv dengan porsi sedang harga sedikit mahal bila dibandingkan dengan rasa nya yg biasa2 saja cenderung kurang enak kuahnya pun kurang panas dengan penyajian yg tidak menarik
Sentiment prediction:  negative
```
